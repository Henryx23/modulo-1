import sys
intentos = 0
maximo_intentos = 2
frase_correcta = "Ábrete Sésamo"
nombre_ladron = input("Introduzca el nombre: ")
while intentos < maximo_intentos:
    frase = input("Introduzca la frase para abrir la puerta: ")
    if frase == frase_correcta:
        print("La puerta se ha abierto")
        sys.exit()
    else:
        print("Incorrecto")
    intentos += 1


print("La puerta se ha cerrado eternamente")
